package main

import (
	"github.com/gin-gonic/gin"
)

func main() {
	router := gin.Default()
	router.GET("/cookie/read", func(context *gin.Context) {
		val, _ := context.Cookie("name")
		context.String(200, "Cookie:%s", val)
	})
	router.GET("/cookie/write", func(context *gin.Context) {
		context.SetCookie("name", "Shimin Li", 60, "/cookie", "localhost", false, true)
	})
	router.GET("/cookie/clear", func(context *gin.Context) {
		context.SetCookie("name", "Bye", -1, "/cookie", "localhost", false, true)
	})
	router.GET("/cookie/overwrite", func(context *gin.Context) {
		context.SetCookie("name", "dewkul", 60, "/cookie", "localhost", false, true)
	})
	// router.GET("/cookie/write/th", func(context *gin.Context) {
	// 	context.SetCookie("country", "TH", 60, "/cookie", "localhost", false, true)
	// })
	// router.GET("/cookie/read/th", func(context *gin.Context) {
	// 	val, _ := context.Cookie("country")
	// 	context.String(200, "Cookie: %s", val)
	// })
	router.Run(":6060")
}
